package sample.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * User: rohitverma
 * Date: 10/08/17
 */
@Service
public class SampleEventServices {
    public static final Logger LOGGER = LoggerFactory.getLogger(SampleEventServices.class);


    public void eventA() {
        LOGGER.info("doing event A here");
    }

    public void eventB() {
        LOGGER.info("doing event B here");
    }


}
