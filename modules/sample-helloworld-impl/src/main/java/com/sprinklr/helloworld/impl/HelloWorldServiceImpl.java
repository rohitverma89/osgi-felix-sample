package com.sprinklr.helloworld.impl;

import com.sprinklr.helloworld.api.HelloWorldService;

public class HelloWorldServiceImpl implements HelloWorldService {

	public void activate() {
		System.out.println("Bundle activated");
		helloWorld();
	}
	
	public void deactivate() {
		System.out.println("Bundle deActivated");
	}
	
	public void helloWorld() {
		System.out.println("Hello World Service");
	}

}
